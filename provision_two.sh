echo "* Add hosts"
echo "192.168.99.101 k8s1.dof.lab k8s1" >> /etc/hosts
echo "192.168.99.102 k8s2.dof.lab k8s2" >> /etc/hosts
echo "192.168.99.103 k8s3.dof.lab k8s3" >> /etc/hosts

echo "* Disable and stop Firewall"
systemctl stop firewalld
systemctl disable firewalld

echo "* Change SELinux state"
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

echo "* Install required packages"
dnf install -y bash-completion wget tc

echo "* Add Repositories"
dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
cat << EOF | tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=0
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

echo "* Install required packages"
dnf install -y https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.13-3.1.el7.x86_64.rpm
dnf install -y docker-ce kubeadm kubectl

echo "* Start Docker"
systemctl enable docker
systemctl start docker

echo "* Start Kubernetes ..."
systemctl enable kubelet
systemctl start kubelet

cat << EOF | tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system

echo "* Turn off swap"
swapoff -a
sed -i '/ swap / s/^/#/' /etc/fstab

echo "* Add user to docker group"
usermod -aG docker vagrant

echo "* Join worker node"
kubeadm join 192.168.99.101:6443 --token abcdef.1234567890abcdef --discovery-token-ca-cert-hash sha256:`cat /vagrant/hash.txt`
 